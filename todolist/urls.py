from django.urls import path

from . import views

# The path() function can recieve four arguments
# We'll focus on the two arguments that are required, which are "route" and "view", and the third argument "name" which allows us to make global changes to the URL patterns of your project while only touching a single file
# Syntax
    # path(route, view, name)
app_name = 'todolist'
urlpatterns = [
    # /todolist route
    path('', views.index, name='index'),
    # /todolist/<todoitem_id> route
    # The <int:todoitem_id> allows for creating a dynamic link where the todoitem_id is provided
    path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
    # /todolist/register
    path('register', views.register, name="register"),
    # /todolist/change_password
    path('change_password', views.change_password, name="change_password"),
    # /todolist/login
    path('login', views.login_view, name="login"),
    # /todolist/logout
    path('logout', views.logout_view, name="logout"),
    path('add_task', views.add_task, name="add_task"),
    path('<int:todoitem_id>/edit', views.update_task, name='update_task'),
    # /todolist/<todoitem_id>/delete route
    path('<int:todoitem_id>/delete', views.delete_task, name='delete_task'),

    path('add_event', views.add_event, name='add_event'),
    path('event_view', views.event_view, name='event_view'),
    path('update_event/<int:event_id>/', views.update_event, name='update_event'),
    path('delete_event/<int:event_id>/', views.delete_event, name='delete_event'),
]